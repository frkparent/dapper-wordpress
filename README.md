# Dapper

Dapper is a professional assortment of adaptable front-end themes and visual improvements for the WordPress Admin Dashboard.

This project is intended for developers and aims to make the popular CMS' Admin Dashboard looks more modern and pleasant, as well as easier to use.

Though it comes with a default theme, you are entirely free (and we encourage you) to build your own exactly the way that you want.



## Features

* Completely frictionless with your WordPress install:
    * The plugin doesn't affect the functionality of the CMS, it purely changes the look;
    * The plugin can be safely disabled inside the "Appearance > Admin Dashboard" menu;
    * Additionally the themes can be assigned per-user inside the user profile page.
* Changes the logo's hyperlink in the Login page to the one of the project
* Adds an accordion system for the menus in the sidebar
* Moves the user card to be visible and easily accessible at all time
* Cleans up and removes many of the distrations of the default UI



## Get Started

If you simply want to install Dapper and use the default theme:

1. Clone this repo inside the `mu-plugins` directory of your project;
2. Install the project's dependencies using `yarn`;
3. Change the `$accent_color` value and the main logo;
4. Build the front-end assets using `yarn run build:production`;
5. Enjoy!

Alternatively if you want to go crazy and express all your creativity (perhaps even making some happy little trees) feel free to create a new theme file.



## Customization

####Color Scheme

The color scheme is dynamically generated based on the `$accent_color` variable inside the default theme: *admin/src/styles/theme_default.scss*
You can simply change this value to your branding color and the color scheme will create the additional colors needed. Otherwise you can manually change the other variables (also in the default theme): `$accent_color2`,  `$accent_color3` and `$accent_color4`

####Logo

The main logo is currently used in the login screen only and is located here: *admin/src/images/logo-main.svg*

####Background Image & Banner

Currently the same image is used for the background in the login page and the top banner in the Admin Dashboard.
This image can be found here: *admin/src/images/bg-admin.jpg*



## File Hierarchy

Dapper uses the same naming convention for the Sass partial files as WordPress do for the stylesheets enqueued in the Admin Dashboard.
This makes it considerably faster to adjust the styles since you can use the Developer Tools to inspect the elements and then find the matching file.

```
dapper/                             # → Root of the plugin
├── admin/                          # → Resources for the admin part of the plugin
│   ├── dist/                       # → Built plugin assets
│   ├── partials/                   # → PHP partials containing markup used by the plugin
│   └── src/                        # → Front-end assets
│       ├── images/                 # → Plugin images (main logo and background)
│       ├── scripts/                # → Plugin scripts
│       └── styles/                 # → Plugin stylesheets
│           ├── core/               # → Custom variables and sass helpers used by the plugin
│           ├── themes/             # → Custom themes for the Admin Dashboard
│           ├── wp-admin/           # → Stylesheet partials matching the "/wp-admin/css/" folder
│           |   ├── common.css      # →
│           |   ├── forms.css       # →
│           |   ├── admin-menu.css  # →
│           |   ├── dashboard.css   # →
│           |   ├── list-tables.css # →
│           |   ├── edit.css        # →
│           |   ├── revisions.css   # →
│           |   ├── media.css       # →
│           |   ├── themes.css      # →
│           |   ├── about.css       # →
│           |   ├── nav-menus.css   # →
│           |   ├── widgets.css     # →
│           |   ├── site-icon.css   # →
│           |   └── l10n.css        # →
│           ├── wp-includes/        # → Stylesheet partials matching the "/wp-includes/css/" folder
│           |   ├── admin-bar.css   # →
│           |   ├── buttons.css     # →
│           |   └── wp-pointer.css  # →
│           └── dapper.scss         # → Entry file for the Sass
├── includes/                       # → Plugin Class files
├── languages/                      # → Translations
├── node_modules/                   # → Node.js packages
├── .eslintrc.js                    # → ESLint configuration file
├── dapper.php                      # → WordPress plugin bootstrap file
├── index.php                       # → Never manually edit
├── postcss.config.js               # → PostCSS configuration file
├── uninstall.php                   # → Functions related to the plugin uninstall process
└── webpack.config.js               # → Webpack configuration file
```



## Screenshots



## Technologies, Frameworks & Tools

* [Sass](http://sass-lang.com/)
* [Webpack](https://webpack.js.org/)



## Build commands

* `yarn run build` — Build the styles in development
* `yarn run build:production` — Build the styles in production
* `yarn run lint:scripts` — Run eslint and test the scripts
* `yarn run lint:styles` — Run style lint and test the styles

Feel free to consult the package.json file for more detail and information.



## Disclaimer

The outrageous specificity level in the stylesheets and the use of `!important` keyword were taken directly out of the original WordPress files and are not my making. I feel it is essential to mention this to clear my mere developer's conscience.


##Credits

Dapper is based on the WordPress Plugin Boilerplate by [Tom McFarlin](http://twitter.com/tommcfarlin/) and currently maintained by [Devin Vinson](https://github.com/DevinVinson/WordPress-Plugin-Boilerplate).

The current version of the Boilerplate was developed in conjunction with [Josh Eaton](https://twitter.com/jjeaton), [Ulrich Pogson](https://twitter.com/grapplerulrich), and [Brad Vincent](https://twitter.com/themergency).



## License

The code is available under the [MIT license](LICENSE.txt).