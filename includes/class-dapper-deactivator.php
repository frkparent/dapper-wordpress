<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://bitbucket.org/frkparent/dapper
 * @since      1.0.0
 *
 * @package    Dapper
 * @subpackage Dapper/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Dapper
 * @subpackage Dapper/includes
 * @author     Frank Parent <frank@redsix.co>
 */
class Dapper_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
