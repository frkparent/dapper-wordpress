<?php

/**
 * Fired during plugin activation
 *
 * @link       https://bitbucket.org/frkparent/dapper
 * @since      1.0.0
 *
 * @package    Dapper
 * @subpackage Dapper/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Dapper
 * @subpackage Dapper/includes
 * @author     Frank Parent <frank@redsix.co>
 */
class Dapper_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
