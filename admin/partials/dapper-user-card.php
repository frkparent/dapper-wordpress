<?php global $current_user; ?>

<div id="user-card">
    <div class="user-card-avatar">
        <a href="<?php echo get_edit_user_link( $current_user->ID ) ?>">
            <?php echo get_avatar( $current_user->ID, 64 ); ?>
        </a>
    </div>

    <div class="user-card-info">
        <a class="user-card-name" href="<?php echo get_edit_user_link( $current_user->ID ) ?>">
            <?php echo $current_user->user_nicename; ?> (<?php echo $current_user->user_email; ?>)
        </a>

        <a class="lnk-edit" href="<?php echo get_edit_user_link( $current_user->ID ) ?>">Edit profile</a>
        <a class="lnk-logout" href="<?= wp_logout_url(); ?>">Logout</a>
    </div>
</div>