<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://bitbucket.org/frkparent/dapper
 * @since      1.0.0
 *
 * @package    Dapper
 * @subpackage Dapper/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Dapper
 * @subpackage Dapper/admin
 * @author     Frank Parent <frank@redsix.co>
 */
class Dapper_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The author of the current site.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $author_name    The string used to uniquely identify the author of this plugin.
	 */
	protected $author_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $author_name, $author_url, $version ) {

		$this->plugin_name = $plugin_name;
		$this->author_name = $author_name;
		$this->author_url = $author_url;
		$this->version = $version;

	}

	/**
	 * Register a "GeneralÉ section for our options page
	 *
	 * @since    1.0.0
	 */
	public function register_setting() {

		add_settings_section(
			$this->plugin_name . '_general',
			__( 'General', 'dapper' ),
			array( $this, $this->plugin_name . '_general_cb' ),
			$this->plugin_name
		);

		// Add the enabled setting in the General tab
		add_settings_field(
			$this->plugin_name . '_enabled',
			__( 'Status', 'dapper' ),
			array( $this, $this->plugin_name . '_enabled_cb' ),
			$this->plugin_name,
			$this->plugin_name . '_general',
			array( 'label_for' => $this->plugin_name . '_enabled' )
		);

		register_setting( $this->plugin_name, $this->plugin_name . '_enabled' );

	}

	/**
	 * Render the text for the general section
	 *
	 * @since  1.0.0
	 */
	public function dapper_general_cb() {
		echo '<p>' . __( 'Manage the settings for the WordPress Admin Dashboard themes.', 'dapper' ) . '</p>';
	}

	/**
	 * Render the radio input field for position option
	 *
	 * @since  1.0.0
	 */
	public function dapper_enabled_cb() {

		$val = get_option( $this->plugin_name . '_enabled' );
		?>

		<fieldset>
			<label>
				<input type="checkbox" name="<?php echo $this->plugin_name . '_enabled' ?>" id="<?php echo $this->plugin_name . '_enabled' ?>" value="true" <?php checked( $val, 'true' ); ?>>
				<?php _e( 'Enable custom themes for the Admin Dashboard', 'dapper' ); ?>
			</label>
		</fieldset>

	<?php }

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Dapper_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Dapper_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		$enabled = get_option( $this->plugin_name . '_enabled', 'true' );

		// Only enqueue the core styles if the option is enabled
		if ( $enabled == 'true' ) {
			wp_enqueue_style( $this->plugin_name . '/fonts', 'https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700' );
			wp_enqueue_style( $this->plugin_name . '/core', plugin_dir_url( __FILE__ ) . 'dist/styles/dapper.css', [$this->plugin_name . '/fonts'], $this->version, 'all' );
		}

		// Then enqueue the default theme everywhere.
		// The user will have the option of selecting it or not in the use rprofile
		wp_enqueue_style( $this->plugin_name . '/theme', plugin_dir_url( __FILE__ ) . 'dist/styles/theme.css', [$this->plugin_name . '/fonts'], $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Dapper_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Dapper_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		$enabled = get_option( $this->plugin_name . '_enabled', 'true' );

		if ( $enabled == 'true' ) {
			wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'dist/scripts/dapper.js', array( 'jquery' ), $this->version, false );
		}

	}


	/**
	 * De-register the default stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function remove_styles() {
		wp_deregister_style('admin-bar');
		wp_deregister_style('buttons');
		wp_deregister_style('wp-admin');
    }


	/**
	 * Enqueue the custom color scheme in the admin
	 *
	 * @return void
	 */
	public function add_custom_color_scheme() {

		// Change the default admin color scheme to include the dapper theme
		add_action('admin_init', function () {
			// Get the theme directory
			$theme_dir = get_template_directory_uri();

			// Name of the new color scheme
			wp_admin_css_color(
				$this->plugin_name . '_theme',
				__( 'Dapper', 'dapper' ),
				plugin_dir_url( __FILE__ ) . 'dist/styles/theme.css',
				array( '#03d152', '#03be63', '#6a6a6a', '#1f1f1f' )
			);
		});

	}


	/**
	 * Change the default color scheme for every users to the new custom one
	 *
	 * @return void
	 */
	public function set_default_user_scheme() {

		add_action( 'user_register', function ($user_id) {
			$args = array(
				'ID' => $user_id,
				'admin_color' => $this->plugin_name . '_theme'
			);
			wp_update_user( $args );
		});

	}


	/**
	 * Render the options page for plugin
	 *
	 * @since  1.0.0
	 */
	public function display_options_page() {

		include_once 'partials/dapper-admin-display.php';

	}

	/**
	 * Add an options page under the Settings submenu
	 *
	 * @since  1.0.0
	 */
	public function add_options_page() {

		add_theme_page(
			__( 'Dapper Settings', 'dapper' ),
			__( 'Dapper', 'dapper' ),
			'manage_options',
			$this->plugin_name,
			array( $this, 'display_options_page' )
		);

	}


	/**
	 * Cleanup helper for the WordPress Admin
	 * Hide the "comments" menu in the admin bar
	 *
	 * @since  1.0.0
	 */
	public function cleanup() {

		/**
		 * Remove the emoji scripts and styles generated by WordPress
		 */
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_action( 'admin_print_styles', 'print_emoji_styles' );
		add_filter( 'emoji_svg_url', '__return_false' );

		add_action( 'wp_before_admin_bar_render', function() {
			global $wp_admin_bar;
			$wp_admin_bar->remove_menu('comments');
		});

		/**
		 * Cleanup the enqueued scripts to remove the "type" attribute that is now
		 * unnecessary and triggers warning on W3C
		 */
		add_filter( 'script_loader_tag', function($tag, $handle) {
			return preg_replace( "/type=['\"]text\/(javascript|css)['\"]/", '', $tag );
		}, 10, 2);

	}

	/**
	 * Change the URL of the login logo to the one of the project
	 *
	 * @since  1.0.0
	 */
	public function login() {

		add_filter('login_headerurl', function () {
			return home_url();
		});

	}


	/**
	 * Output the markup for the user card in the top left corner of the dashboard
	 * The user card includes the profile image and some useful links
	 *
	 * @since  1.0.0
	 */
	public function user_card() {

		add_action('admin_head', array($this, 'display_user_card') );

	}

	/**
	 * Render the user card in the header
	 *
	 * @since  1.0.0
	 */
	public function display_user_card() {

		include_once 'partials/dapper-user-card.php';

	}


	/**
	 * This will remove all of the meta_boxes in the Dashboard page
	 */
	function cleanup_dashboard() {

		remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal'); //Removes the 'incoming links' widget
		remove_meta_box('dashboard_plugins', 'dashboard', 'normal'); //Removes the 'plugins' widget
		remove_meta_box('dashboard_primary', 'dashboard', 'normal'); //Removes the 'WordPress News' widget
		remove_meta_box('dashboard_secondary', 'dashboard', 'normal'); //Removes the secondary widget
		remove_meta_box('dashboard_quick_press', 'dashboard', 'side'); //Removes the 'Quick Draft' widget
		remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side'); //Removes the 'Recent Drafts' widget
		remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal'); //Removes the 'Activity' widget
		remove_meta_box('dashboard_right_now', 'dashboard', 'normal'); //Removes the 'At a Glance' widget
		remove_meta_box('dashboard_activity', 'dashboard', 'normal'); //Removes the 'Activity' widget (since 3.8)

	}

}
