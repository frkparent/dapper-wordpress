/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./scripts/components/cleanup.js":
/*!***************************************!*\
  !*** ./scripts/components/cleanup.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/***/ }),

/***/ "./scripts/components/navigation.js":
/*!******************************************!*\
  !*** ./scripts/components/navigation.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($) {

function toggleMenu($el) {
  $el.parent().toggleClass('opened').find('.wp-submenu').slideToggle(150);
}

$(function () {
  var $leftNav = $('#adminmenu');
  var $topMenus = $leftNav.find('> li.wp-has-submenu');
  var $subMenus = $topMenus.find('> a');
  var $currentSubMenu = $leftNav.find('> .wp-has-current-submenu > a'); // Click on the menu in the sidebar

  $subMenus.on('click', function (e) {
    var $this = $(this);
    var $exceptions = $this.add($currentSubMenu);
    e.preventDefault(); // If the user clicks on the current active menu, toggle only
    // the clicked menus and not the others

    if ($this.hasClass('wp-has-current-submenu')) {
      toggleMenu($this);
      return;
    } // Otherwise close the other opened menus EXCEPT the clicked one AND
    // the one from the current page


    $subMenus.not($exceptions).parent().removeClass('opened').find('.wp-submenu').slideUp(150); // And then show the selected one

    toggleMenu($this);
  });
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "jquery")))

/***/ }),

/***/ "./scripts/dapper.js":
/*!***************************!*\
  !*** ./scripts/dapper.js ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(/*! ./components/cleanup */ "./scripts/components/cleanup.js");

__webpack_require__(/*! ./components/navigation */ "./scripts/components/navigation.js");

/***/ }),

/***/ "./styles/dapper.scss":
/*!****************************!*\
  !*** ./styles/dapper.scss ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!******************************************************!*\
  !*** multi ./scripts/dapper.js ./styles/dapper.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ./scripts/dapper.js */"./scripts/dapper.js");
module.exports = __webpack_require__(/*! ./styles/dapper.scss */"./styles/dapper.scss");


/***/ }),

/***/ "jquery":
/*!*************************!*\
  !*** external "jQuery" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = jQuery;

/***/ })

/******/ });
//# sourceMappingURL=dapper.js.map