function toggleMenu( $el ) {
    $el.parent().toggleClass( 'opened' ).find( '.wp-submenu' ).slideToggle( 150 )
}


$(() => {
    let $leftNav = $( '#adminmenu' );
    let $topMenus = $leftNav.find( '> li.wp-has-submenu' );
    let $subMenus = $topMenus.find( '> a' );
    let $currentSubMenu = $leftNav.find( '> .wp-has-current-submenu > a' );

    // Click on the menu in the sidebar
    $subMenus.on( 'click', function(e) {
        let $this = $( this );
        let $exceptions = $this.add( $currentSubMenu );

        e.preventDefault();

        // If the user clicks on the current active menu, toggle only
        // the clicked menus and not the others
        if ( $this.hasClass('wp-has-current-submenu') ) {
            toggleMenu( $this );
            return;
        }

        // Otherwise close the other opened menus EXCEPT the clicked one AND
        // the one from the current page
        $subMenus
            .not( $exceptions ).parent().removeClass( 'opened' )
            .find( '.wp-submenu' ).slideUp( 150 );

        // And then show the selected one
        toggleMenu( $this );
    });
});