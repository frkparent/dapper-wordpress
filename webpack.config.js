'use strict';

const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const { argv } = require('yargs');

const isProduction = !!((argv.env && argv.env.production) || argv.p);

const config = {
    context: path.resolve(__dirname, 'admin/src'),
    entry: {
        dapper: ['./scripts/dapper.js', './styles/dapper.scss'],
        theme: ['./styles/theme_default.scss'],
    },
    output: {
        path: path.resolve(__dirname, 'admin/dist/scripts'),
        filename: '[name].js',
    },
    devtool: (!isProduction ? '#source-map' : undefined),
    module: {
        rules: [
        {
            enforce: 'pre',
            test: /\.js$/,
            exclude: /node_modules/,
            loader: "eslint-loader",
        },
        {
            test: /\.css$/,
            use: ExtractTextPlugin.extract({
                use: [
                    { loader: 'css-loader', options: { sourceMap: !isProduction } },
                    { loader: 'postcss-loader', options: { sourceMap: !isProduction } },
                ],
            }),
        },
        {
            test: /\.scss$/,
            use: ExtractTextPlugin.extract({
                use: [
                    { loader: 'css-loader', options: { sourceMap: !isProduction } },
                    { loader: 'postcss-loader', options: { sourceMap: !isProduction } },
                    { loader: 'sass-loader', options: { sourceMap: !isProduction } },
                ],
            }),
        },
        {
            test: /\.js$/,
            exclude: /(node_modules|bower_components)/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: [
                        ['@babel/preset-env'],
                    ],
                },
            },
        },
        {
            test: /\.(gif|svg|jpg|png)$/,
            loader: 'file-loader',
            options: {
                name: '../[path][name].[ext]',
            },
        },
        ],
    },
    externals: {
        jquery: 'jQuery',
    },
    plugins: [
        new CleanWebpackPlugin([path.resolve(__dirname, 'admin/dist')]),
        new webpack.LoaderOptionsPlugin({
            minimize: isProduction ? true : false,
            stats: { colors: true },
        }),
         new ExtractTextPlugin({
            filename: '../styles/[name].css',
            allChunks: true,
        }),
        new webpack.LoaderOptionsPlugin({
            test: /\.js$/,
            options: {
                eslint: { failOnWarning: false, failOnError: true },
            },
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
        }),
        new StyleLintPlugin({
            failOnError: false,
            emitErrors: true,
            quiet: false,
            syntax: 'scss',
        }),
    ],
};

module.exports = config;