/* eslint-disable */

const { argv } = require('yargs');
const isProduction = !!((argv.env && argv.env.production) || argv.p);

const cssnanoConfig = {
    preset: [ 'default', {
        discardComments: { removeAll: true }
    }]
};

module.exports = ({ file, options }) => {
    return {
        parser: isProduction ? 'postcss-safe-parser' : undefined,
        plugins: {
            cssnano: isProduction ? cssnanoConfig : false,
            autoprefixer: true,
        },
    };
};
